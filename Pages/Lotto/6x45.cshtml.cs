using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Lottery.Models;

namespace Lottery.Pages {
  public class _6x45Model : PageModel {
    public _6x45Model() {
      Numbers = Generator.LottoNumbers(6, 45);
    }
    public void OnGet() {
    }

    public void OnPost() {

    }

    public SortedSet<int> Numbers { get; set; }
  }
}
