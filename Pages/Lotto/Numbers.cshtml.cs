using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Lottery.Models;

namespace Lottery.Pages
{
    public class NumbersModel : PageModel
    {
        public NumbersModel()
        {
            Numbers = null;
            
        }
        public void OnGet(int type, int maxNumbers)
        {
            Numbers = Generator.LottoNumbers(type, maxNumbers);
        }

        public SortedSet<int> Numbers { get; set; }
    }
}
